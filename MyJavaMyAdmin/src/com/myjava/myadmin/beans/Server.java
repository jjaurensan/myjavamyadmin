package com.myjava.myadmin.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.LocalBean;
import javax.enterprise.context.SessionScoped;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.mysql.cj.xdevapi.Schema;
import com.mysql.cj.xdevapi.Table;

@LocalBean
@SessionScoped
public class Server implements Serializable {

	private static final long serialVersionUID = 1L;

		private String adressServer ="mysqlx://localhost:33060";
		private TreeNode serverTree;
		//private TreeNode selectedNode;
	
		private HashMap<Schema, TreeNode> node = new HashMap<>();
		private List<Schema> listSchema;
		   
		public void initTreeview() {
		    	serverTree = new DefaultTreeNode("Serveur","DBserver", null);
		    	
		    	for (Schema schema : listSchema) {
		    		node.put(schema, new DefaultTreeNode("schema",schema.getName(), serverTree));	
					
		    		if (schema.getTables().size()!=0) {
		    			List<Table> tables =  new ArrayList<Table>();
		    			List<Table> views = new ArrayList<Table>();
		    			for(Table schemaGetTable : schema.getTables()) {
		    				if (schemaGetTable.isView()) {
								views.add(schemaGetTable);
							}else {
								tables.add(schemaGetTable);
							} 
						}
		    			if(!views.isEmpty()) {
		    				DefaultTreeNode nodeTables = new DefaultTreeNode("tables","Tables", node.get(schema));
		    				DefaultTreeNode nodeViews = new DefaultTreeNode("views","Views", node.get(schema));
		    				for (Table table : tables) {
			    				new DefaultTreeNode("table",table.getName(), nodeTables);
							}
		    				for (Table view : views) {
			    				new DefaultTreeNode("view",view.getName(), nodeViews);
							}
		    			}else {
		    				for (Table table : tables) {
			    				new DefaultTreeNode("table",table.getName(), node.get(schema));
							}
		    			}
		    				
					}		    		
		    	}	    	
		    }
		    

		public List<Schema> getListSchema() {
			return listSchema;
		}

		public void setListSchema(List<Schema> listSchema) {
			this.listSchema = listSchema;
		}

		public String getAdressServer() {
			return adressServer;
		}

		public void setAdressServer(String adressServer) {
			this.adressServer = adressServer;
		}

		public TreeNode getRoot() {
	        return serverTree;
	    }

		public void toInitSchemas(List<Schema> schemas) {
			this.listSchema=schemas;				
		}
//			public TreeNode getSelectedNode() {
//		        return selectedNode;
//		    }
//		 
//		    public void setSelectedNode(TreeNode selectedNode) {
//		        this.selectedNode = selectedNode;
//		    }
//		    
		   
}
