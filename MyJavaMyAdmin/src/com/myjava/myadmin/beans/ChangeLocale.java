package com.myjava.myadmin.beans;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@SessionScoped
@Named("ChangeLocale")
public class ChangeLocale implements Serializable {

	private static final long serialVersionUID = 1L;
	
	// la locale des pages
	private String locale="fr";

	//CONSTRUCT
	public ChangeLocale() {
		
	}
	
	//METHODS
	
	public void setFrenchLocale() {
		locale="fr";
		
	}
	public void setEnglishLocale() {
		locale="en";
		
	}
	
	
	//GET & SET
	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}
	
	

}
