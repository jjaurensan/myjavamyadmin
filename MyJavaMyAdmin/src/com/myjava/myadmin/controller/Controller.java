package com.myjava.myadmin.controller;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.LoggerFactory;

import com.myjava.myadmin.beans.User;
import com.myjava.myadmin.beans.Server;

import com.mysql.cj.xdevapi.Client;
import com.mysql.cj.xdevapi.ClientFactory;
import com.mysql.cj.xdevapi.RowResult;
import com.mysql.cj.xdevapi.Schema;
import com.mysql.cj.xdevapi.Session;
import com.mysql.cj.xdevapi.Table;
import com.mysql.cj.xdevapi.Row;
import com.mysql.cj.xdevapi.SelectStatement;

@SessionScoped
@Named("Controller")
public class Controller implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final org.slf4j.Logger LOGGER_SL4J = LoggerFactory.getLogger(Controller.class.getName());
	
	@Inject
	private User user;
	@Inject
	private Server server;	
		
	private Client client;
	private Session session;
	private Schema currentSchema;
	private Table currentTable;
	private RowResult resultat;
	private List<Row> rows;
		
	@PostConstruct
	public void init() {
		LOGGER_SL4J.info("Controller init");		
	}
	
	public Controller() {		
		LOGGER_SL4J.info("Controller construct OK");
		
	}
	
	public void createClient() {
		ClientFactory clientFactory = new ClientFactory();
		this.client= clientFactory.getClient(this.server.getAdressServer()+"?user="+this.user.getUserName()+"&password="+this.user.getPassWord()+"",
				"{\"pooling\":{\"enabled\":true, \"maxSize\":8, \"maxIdleTime\":30000, \"queueTimeout\":10000} }");
	}
	
	public String index() {		
		LOGGER_SL4J.info("Controller index OK");
		return "index";
	}
	
	public String oneSchema(String schemaName) {
		this.currentSchema=this.session.getSchema(schemaName);
		return "oneschema?faces-redirect=true";		
	}	

	public String infoTable(String table) {
		this.currentTable=this.currentSchema.getTable(table);
		SelectStatement stmt = this.currentTable.select();
		this.resultat = stmt.execute();		
		this.rows=this.resultat.fetchAll();		
		return "infotable?faces-redirect=true";	
	}
	
	
	public String toLogIn() throws SQLException {
		FacesMessage message =new FacesMessage("Login OK");
		FacesContext.getCurrentInstance().addMessage(null, message);
		System.out.println("login Ok, username " + this.user.getUserName());
		this.createClient();
		this.session=this.client.getSession();		
		this.server.toInitSchemas(this.session.getSchemas());
		this.server.initTreeview();
		return "index?faces-redirect=true";
	}
	
	public Schema getCurrentSchema() {
		return currentSchema;
	}
	public void setCurrentSchema(Schema currentSchema) {
		this.currentSchema = currentSchema;
	}
	public Table getCurrentTable() {
		return currentTable;
	}
	public void setCurrentTable(Table currentTable) {
		this.currentTable = currentTable;		
	}
	public RowResult getResultat() {
		return resultat;
	}
	public void setResultat(RowResult resultat) {
		this.resultat = resultat;
	}
	public User getUser() {
		return user;
	}
	public Server getServer() {
		return server;
	}

	public List<Row> getRows() {
		return rows;
	}

	public void setRows(List<Row> rows) {
		this.rows = rows;
	}
	
	
}
