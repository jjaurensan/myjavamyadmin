package com.myjava.myadmin.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.event.RowEditEvent;

@SessionScoped
@Named("dtEditView")
public class EditViewTable implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public void onRowEdit(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Column Edited", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
     
    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
}
